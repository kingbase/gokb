// +build go1.10

package gokb_test

import (
	"database/sql"
	"fmt"

	"gitea.com/kingbase/gokb"
)

func ExampleNewConnector() {
	name := ""
	connector, err := gokb.NewConnector(name)
	if err != nil {
		fmt.Println(err)
		return
	}
	db := sql.OpenDB(connector)
	defer db.Close()

	// Use the DB
	txn, err := db.Begin()
	if err != nil {
		fmt.Println(err)
		return
	}
	txn.Rollback()
}
